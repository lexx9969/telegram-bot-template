import re
import logging
import time
import requests
import urllib.parse
from os import environ
from storage.bot_info import BotInfo
from storage.stickers.stickers import Stickers


class Bot:
    def __init__(self, bot_id, api_key):
        self.bot_id = bot_id
        self.api_key = api_key
        self.url = 'https://api.telegram.org/bot{bot_id}:{api_key}'.format(bot_id=bot_id, api_key=api_key)

    def send_message(self, chat_id, text, reply_markup=None):
        resp = None
        try:
            nowtime = time.time()
            parameters = {
                'chat_id': chat_id,
                'text': text
            }
            if reply_markup:
                parameters['reply_markup'] = reply_markup
            query_parameters = "&".join(
                [item + '=' + urllib.parse.quote_plus(str(parameters[item])) for item in parameters])
            req = requests.get(self.url + "/sendMessage?" + query_parameters)
            logging.debug('Telegram sendMessage request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp

    def send_sticker(self, chat_id, sticker_id):
        resp = None
        try:
            nowtime = time.time()
            req = requests.get(self.url + '/sendSticker?chat_id={chat_id}&sticker={sticker}'.format(chat_id=chat_id,
                                                                                                    sticker=sticker_id))
            logging.debug('Telegram sendSticker request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp

    def send_image(self, chat_id, image_id):
        resp = None
        try:
            nowtime = time.time()
            req = requests.get(self.url + '/sendPhoto?chat_id={chat_id}&photo={photo}'.format(chat_id=chat_id,
                                                                                              photo=image_id))
            logging.debug('Telegram sendPhoto request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp

    def send_audio(self, chat_id, audio):
        resp = None
        try:
            nowtime = time.time()
            req = requests.get(self.url + '/sendAudio?chat_id={chat_id}&audio={audio}'.format(chat_id=chat_id,
                                                                                              audio=audio))
            logging.debug('Telegram sendAudio request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp


class Actions:
    def __init__(self, text, chat_id, username):
        self.username = username
        self.text = text
        self.chat_id = chat_id
        self.command = text.split(' ')[0].lower() if len(text.split(' ')) else None
        self.tagged_members = re.findall(r'\@\w+', self.text)
        self.bot = Bot(environ.get('TELEGRAM_BOT_ID'), environ.get('TELEGRAM_API_KEY'))
        self.stickers = Stickers()
        self.bot_info = BotInfo()

    def is_command_exists(self):
        return True if self.command else False

    def _get_title(self):
        title = self.text.replace(self.command, '')
        title = re.sub(r'\@\w+', '', title)
        return title.strip()

    def _press_f(self):
        self.bot.send_sticker(self.chat_id, self.stickers.get_f())

    def _show_info(self):
        self.bot.send_message(self.chat_id, self.bot_info.get())

    def dispatch(self):
        if self.command in ['/info']:
            self._show_info()
        elif self.command in ['/f']:
            self._press_f()
