import logging
from os import environ
from app.utils.bot import Bot, Actions
from storage.stickers.stickers import Stickers


class CommandProcessor:
    @staticmethod
    def telegram_command(data):
        try:
            if 'message' in data and 'text' in data['message']:
                bot = Bot(environ.get('TELEGRAM_BOT_ID'), environ.get('TELEGRAM_API_KEY'))
                stickers = Stickers()
                message = data['message']['text']
                chat_id = data['message']['chat']['id']
                username = data['message']['from']['username']
                try:
                    actions = Actions(message, chat_id, username)
                    if actions.is_command_exists():
                        actions.dispatch()
                except ValueError:
                    bot.send_sticker(chat_id, stickers.get_fail())
                    bot.send_message(
                        chat_id,
                        'Не смогли вас понять :('
                    )
            else:
                logging.info('Receive incorrect data payload')
        except Exception as exc:
            logging.exception(exc)
